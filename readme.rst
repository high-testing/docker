############
Docker Stuff
############

===========
Dockerfiles
===========

Keep all ``Dockerfiles`` here.

Push to: https://hub.docker.com/r/hightesting/

.. warning::

    Docker images are **public**, do not upload sensistive or customer info!

--------
Building
--------

Run from root of repository::

  docker build ./<path_to_dockerfile> -t hightesting/<directory_name>

For example::

  docker build dockerfiles/geo_django -t hightesting/geo_django
